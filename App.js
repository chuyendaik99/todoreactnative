import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TextInput,
  FlatList,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import Header from './src/components/Header';
import Task from './src/components/Task';
import Footer from './src/components/Footer';

const App = () => {
  const [value, setValue] = useState('');
  const [todos, setTodos] = useState([]);

  const countTasks = todos.length;
  const countTaskCompleted = todos.filter(todo=>todo.checked).length ;
  const countOpen = todos.filter(todo=>!todo.checked).length ;

  handleAddTodo = async () => {
    if (value.length > 0) {
      setTodos([...todos, {text: value, key: Date.now(), checked: false}]);
      setValue('');
    } else {
      Alert.alert('Bạn hãy nhập task nhé');
    }
  };

  const onDelete = (id) => {
    setTodos(
      todos.filter(todo => {
        return todo.key !== id;
      }),
    );
  };

  const onCheck = (id) => {
    setTodos(
      todos.map(todo => {
        if (todo.key === id) todo.checked = !todo.checked;
        return todo;
      }),
    );
  };

  updateInputValue = (newValue) => {
    setValue(newValue);
  }

  return (
    <View style={[styles.container]}>
      <Header />
      <View style={styles.textInputContainer}>
        <TextInput
          style={styles.textInput}
          multiline={true}
          onChangeText={updateInputValue}
          placeholder={'Add Task...'}
          placeholderTextColor="gray"
          value={value}
        />
        <TouchableOpacity onPress={handleAddTodo}>
          <Icon name="edit" size={30} color="gray" style={{marginLeft: 15}} />
        </TouchableOpacity>
      </View>
      <>
        {todos.length > 0 ? (
          <FlatList
            data={todos}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => {
              return (
                <Task
                  text={item.text}
                  key={item.key}
                  checked={item.checked}
                  item={item}
                  onCheck={onCheck}
                  onDelete={onDelete}
                />
              );
            }}/>
        ) : (
          <Text style={styles.nothingTask}>You're all caught up!</Text>
        )}
      </>

      <Footer
        countTasks={countTasks}
        countTaskCompleted={countTaskCompleted}
        countOpen={countOpen}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    flex: 1,
    padding: 2,
  },
  textInput: {
    // height: 20,
    flex: 1,
    // minHeight: '10%',
    paddingTop: 10,
    fontSize: 25,
    color: 'black',
    paddingLeft: 10,
  },
  textInputContainer: {
    flexDirection: 'row',
    alignItems: 'baseline',
    borderColor: 'rgb(222,222,222)',
    borderBottomWidth: 2,
    borderBottomColor: 'pink',
    paddingRight: 10,
    paddingBottom: 5,
  },
  nothingTask: {
    textAlign: 'center',
    flex: 3,
    paddingTop: 250,
    fontWeight: 'bold',
    fontSize: 26,
    color: 'gray',
  },
});

export default App;
