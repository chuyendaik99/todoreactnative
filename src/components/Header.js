import React, {useState, useEffect} from 'react';
import {StyleSheet, Image, Text, SafeAreaView} from 'react-native';

const Header = () => {
  const [currentDate, setCurrentDate] = useState('');
  const [currentMonth, setCurrentMonth] = useState('');
  const [currentHour, setCurrentHour] = useState('');
  
  const getHeaderImageClass = currentHour => {
    if (currentHour >= 6 && currentHour < 16) {
      return require('../../assets/health.jpeg');
    } else if (currentHour >= 16 && currentHour < 20) {
      return require('../../assets/header-afternoon.jpeg');
    } else if (currentHour >= 20 || currentHour <= 5) {
      return require('../../assets/header-night.jpeg');
    }
  };

  useEffect(() => {
    const day = new Date();
    const date = day.getDate();
    const month = day.getMonth() + 1;
    const currentHour = day.getHours();
    const weekDay = [
      'Chủ nhật',
      'Thứ 2',
      'Thứ 3',
      'Thứ 4',
      'Thứ 5',
      'Thứ 6',
      'Thứ 7',
    ][day.getDay()];

    setCurrentHour(currentHour);
    setCurrentDate(weekDay + ' ngày ' + date);
    setCurrentMonth('Tháng ' + month);
  }, []);

  return (
    <SafeAreaView style={[styles.container]}>
      <Image
        source={getHeaderImageClass(currentHour)}
        style={styles.imageHeader}
      />

      <Text style={styles.dateHeader}>{currentDate}</Text>
      <Text style={styles.monthHeader}>{currentMonth}</Text>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  imageHeader: {
    flex: 1,
    width: 390,
    height: 150,
  },
  dateHeader: {
    color: 'white',
    position: 'absolute',
    fontSize: 25,
    paddingTop: 60,
    paddingLeft: 10,
  },
  monthHeader: {
    color: 'white',
    position: 'absolute',
    fontSize: 25,
    paddingTop: 90,
    paddingLeft: 20,
  },
});

export default Header;
