import React from 'react';
import {View, TouchableOpacity, Text, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

export default function Task(props) {
  const {item, onCheck, onDelete, checked} = props;
  return (
    <View style={styles.taskWrapper}>
      <TouchableOpacity onPress={() => onCheck(item.key)}>
        <Icon
          name={checked ? 'check-square' : 'square'}
          size={30}
          color={checked ? '#ff1493' : 'black'}
          style={{marginLeft: 15}}
        />
      </TouchableOpacity>

      <View>
        {checked ? (
           <Text style={styles.taskDone}>{item.text}</Text> 
         ) : ( 
          <Text style={styles.task}>{item.text}</Text>
         )} 
      </View>
      <Icon
        name="trash-2"
        size={30}
        color="gray"
        style={{marginLeft: 'auto'}}
        onPress={() => onDelete(item.key)}
      />
    </View>
  );
}
const styles = StyleSheet.create({
  taskWrapper: {
    marginTop: '5%',
    flexDirection: 'row',
    borderColor: '#FFFFFF',
    borderBottomWidth: 1.5,
    width: '100%',
    // alignItems: 'stretch',
    minHeight: 40,
    paddingRight: 10,
  },
  task: {
    paddingBottom: 20,
    paddingLeft: 10,
    marginTop: 4,
    borderColor: '#F0F0F0',
    borderBottomWidth: 1,
    fontSize: 17,
    fontWeight: 'bold',
    color: 'black',
  },
  taskDone: {
    paddingBottom: 20,
    paddingLeft: 10,
    marginTop: 4,
    borderColor: '#F0F0F0',
    borderBottomWidth: 1,
    fontSize: 17,
    textDecorationLine: 'line-through',
    color: '#ff1493',
  },
});
