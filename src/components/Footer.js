import React from 'react';
import {StyleSheet, Text, SafeAreaView} from 'react-native';

const Footer = props => (
  <SafeAreaView style={[styles.container]}>
    <Text style={styles.textFooter}>{props.countTasks} Tasks</Text>
    <Text style={styles.textFooter}>{props.countTaskCompleted} Complete</Text>
    <Text style={styles.textFooter}>{props.countOpen} Open</Text>
  </SafeAreaView>
);

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#dcdcdc',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  textFooter: {
    marginTop: 7,
  },
});

export default Footer;
